# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

# SQL Server 2017 RC1 and above
from revoscalepy import RxComputeContext, RxInSqlServer, RxSqlServerData
from revoscalepy import rx_import

connection_string = 'Driver={ODBC Driver 13 for SQL Server};Server=13.70.64.119,1433;Database=TutorialDB;Uid=CanonCSE;Pwd=C@n0nCSEP@55w0rd;'

column_info = {
    "Year" : { "type" : "integer" },
    "Month" : { "type" : "integer" },
    "Day" : { "type" : "integer" },
    "RentalCount" : { "type" : "integer" },
    "WeekDay" : {
        "type" : "factor",
        "levels" : ["1", "2", "3", "4", "5", "6", "7"]
    },
    "Holiday" : {
        "type" : "factor",
        "levels" : ["1", "0"]
    },
    "Snow" : {
        "type" : "factor",
        "levels" : ["1", "0"]
    }
}

data_source = RxSqlServerData(table = "dbo.rental_data", connection_string = connection_string, column_info = column_info)

df = pd.DataFrame(rx_import(input_data = data_source))

df.dtypes

df.head()

df.tail()

summary = df.describe(include = 'all')
summary

# > %matplotlib inline

year = df['Year']
month = df['Month']
day = df['Day']
rentalCount = df['RentalCount']
weekDay = df['WeekDay']
holiday = df['Holiday']
snow = df['Snow']
fHoliday = df['FHoliday']
fSnow = df['FSnow']
fWeekDay = df['FWeekDay']

figure = plt.figure(figsize=(20, 5))
plt.subplot(131), plt.hist(year), plt.xticks(list(set(year)))
plt.subplot(132), plt.hist(month), plt.xticks(list(set(month)))
plt.show()

df[['RentalCount', 'FWeekDay']].boxplot(by = 'FWeekDay', figsize=(10, 10))
plt.show()


# T-SQL - Five Number Summary

import pickle
import pyodbc

sql = "EXECUTE [dbo].[FiveNumberSummary]"
connection = pyodbc.connect(connection_string)
result = pd.read_sql(sql, connection)
connection.close()
result.columns = df.columns[0:7]
# Interesting here that pandas.DataFrame.describe returns different rows when used in SQL Server...
result.index = [summary.index[0], summary.index[4], summary.index[5], summary.index[6], summary.index[7], summary.index[8], summary.index[9], summary.index[10]]
result

# T-SQL - Box Plot

sql = "EXECUTE [dbo].[BoxPlot]"
connection = pyodbc.connect(connection_string)
cursor = connection.cursor()
cursor.execute(sql)
tables = cursor.fetchall()
connection.close()

# > %matplotlib qt5

for i in range(0, len(tables)):
    figure = pickle.loads(tables[i][0])
    print(type(figure))

# Linear Regression
    
columns = df.columns.tolist()

columns = [c for c in columns if c not in ["Year"]]

target = "RentalCount"

train = df.sample(frac = 0.8, random_state = 1)

print("Training set shape:", train.shape)

test = df.loc[~df.index.isin(train.index)]

print("Testing set shape:", test.shape)

linearRegression = LinearRegression()

linearRegression.fit(train[columns], train[target])

linearRegression_predict = linearRegression.predict(test[columns])

print("Predictions:", linearRegression_predict)

linearRegression_mse = mean_squared_error(linearRegression_predict, test[target])

print("Computed error:", linearRegression_mse)