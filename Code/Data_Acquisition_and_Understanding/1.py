#%%
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
#%%

#%%
# SQL Server 2017 RC1 and above
from revoscalepy import RxComputeContext, RxInSqlServer, RxSqlServerData
from revoscalepy import rx_import
#%%

#%%
connection_string = 'Driver=SQL Server;Server=.\SQLEXPRESS;Database=TutorialDB;Integrated Security=True;'

column_info = {
    "Year" : { "type" : "integer" },
    "Month" : { "type" : "integer" },
    "Day" : { "type" : "integer" },
    "RentalCount" : { "type" : "integer" },
    "WeekDay" : {
        "type" : "factor",
        "levels" : ["1", "2", "3", "4", "5", "6", "7"]
    },
    "Holiday" : {
        "type" : "factor",
        "levels" : ["1", "0"]
    },
    "Snow" : {
        "type" : "factor",
        "levels" : ["1", "0"]
    }
}

data_source = RxSqlServerData(table = "dbo.rental_data", connection_string = connection_string, column_info = column_info)

type(data_source)

computeContext = RxInSqlServer(
     connection_string = connection_string,
     num_tasks = 1,
     auto_cleanup = False
)

df = rx_import(input_data = data_source)
#%%

#%%
df.dtypes

df.head()

df.tail()

summary = df.describe(include = 'all')
summary
#%%