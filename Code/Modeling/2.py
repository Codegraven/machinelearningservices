#%%
columns = df.columns.tolist()

columns = [c for c in columns if c not in ["Year"]]
#%%

#%%
target = "RentalCount"

train = df.sample(frac = 0.8, random_state = 1)

print("Training set shape:", train.shape)

test = df.loc[~df.index.isin(train.index)]

print("Testing set shape:", test.shape)
#%%

#%%
linearRegression = LinearRegression()

linearRegression.fit(train[columns], train[target])
#%%

#%%
linearRegression_predict = linearRegression.predict(test[columns])

print("Predictions:", linearRegression_predict)

linearRegression_mse = mean_squared_error(linearRegression_predict, test[target])

print("Computed error:", linearRegression_mse)
#%%